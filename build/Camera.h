/*#ifndef CAMERA_H
#define CAMERA_H

#include <OgreCamera.h>
#include <OgreSceneNode.h>

class Camera
{
public:
	Camera();
	virtual ~Camera();

	void update(float dt);

	Ogre::SceneNode* getNode() { return node; }
	Ogre::Camera* getOgreCamera() { return cam; }

private:
	void initViewports();

	float spd, turn_spd, pitch_spd;

	Ogre::Camera* cam;
	Ogre::SceneNode* node;
	Ogre::SceneNode* cam_node;
	Ogre::SceneNode* pitch_node;
};

Camera::Camera()
	: spd(10),
	turn_spd(12),
	pitch_spd(4),
	node(0),
	cam_node(0),
	pitch_node(0)
{
	cam = GameManager::instance().scn_mgr->createCamera("UserCamera");
	cam->setNearClipDistance(.1);
	node = GameManager::instance().scn_mgr->getRootSceneNode()->createChildSceneNode();

	cam_node = node->createChildSceneNode();
	cam_node->setPosition(0, 1.8, 3);
	pitch_node = cam_node->createChildSceneNode();
	pitch_node->attachObject(cam);

	initViewports();
}

void Camera::initViewports()
{
	Ogre::Viewport* vp =
		GameManager::instance().window->addViewport(cam);
	vp->setBackgroundColour(Ogre::ColourValue(0, 0, 0));

	cam->setAspectRatio(
		Ogre::Real(vp->getActualWidth()) /
		Ogre::Real(vp->getActualHeight()));
}